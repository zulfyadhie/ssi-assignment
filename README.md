# Technology used
* Spring boot
* Spring data JPA
* Hibernate
* HikariCP
* Spring MVC
* Springfox

# Notes
This source code is deployed in
http://128.199.120.142

REST documentation is genereated using swagger.

For create and update, using the same method but different JSON data. For Category, the JSON format is


```
#!json

{
  "id": 1,
  "name": "Test Item",
  "parentId": 50
}
```


"id" and "parentId" is optional. For example creating new category with no parent using JSON below


```
#!json

 {
   "name": "New category name here"
 }
```

 
 
 And for item the format is similar, the JSON format
 

```
#!json

 {
   "categoryId": 1,
   "color": "yellow",
   "id": 1,
   "name": "Item 1",
   "price": 1000,
   "size": "XXL"
 }
```

 
 for create, just remove the "id"
 

```
#!json

 {
    "categoryId": 1,
    "color": "yellow",   
    "name": "Item 1",
    "price": 1000,
    "size": "XXL"
  }
```