package com.ssi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsiAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsiAssignmentApplication.class, args);
	}
}
