package com.ssi.controller;

import com.ssi.application.category.CategoryApplicationService;
import com.ssi.application.category.model.CategoryListModel;
import com.ssi.application.category.model.CategoryModel;
import com.ssi.application.item.ItemApplicationService;
import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.application.item.model.ItemListModel;
import com.ssi.application.item.model.ItemModel;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
@RestController
@RequestMapping(value = "/catalog")
@Api(basePath = "/catalog", value = "catalog", description = "Operations with catalog", produces = "application/json")
public class CatalogController {

    @Autowired
    private CategoryApplicationService categoryApplicationService;
    @Autowired
    private ItemApplicationService itemApplicationService;

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public List<CategoryListModel> categoryList(){
        return categoryApplicationService.getList();
    }

    @RequestMapping(value = "/category", method = {RequestMethod.POST, RequestMethod.PUT})
    public CategoryListModel categorySaveOrUpdate(@RequestBody CategoryModel categoryModel){
        return categoryApplicationService.saveOrUpdate(categoryModel);
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.DELETE)
    public void categoryDelete(@PathVariable Long id){
        categoryApplicationService.delete(id);
    }

    @RequestMapping(value = "/category/{id}/child", method = RequestMethod.GET)
    public List<CategoryListModel> categoryChild(@PathVariable Long id){
        return categoryApplicationService.getChild(id);
    }

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public List<ItemListModel> itemList(@RequestParam(value = "color", required = false) String color, @RequestParam(value = "size", required = false) String size, @RequestParam(value = "priceFrom", required = false) Long priceFrom, @RequestParam(value = "priceTo", required = false) Long priceTo){
        return itemApplicationService.getList(new ItemFilterModel(size, color, priceFrom, priceTo));
    }

    @RequestMapping(value = "/item", method = {RequestMethod.POST, RequestMethod.PUT})
    public ItemListModel itemSaveOrUpdate(@RequestBody ItemModel itemModel){
        return itemApplicationService.saveOrUpdate(itemModel);
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.DELETE)
    public void itemDelete(@PathVariable Long id){
        itemApplicationService.delete(id);
    }

}