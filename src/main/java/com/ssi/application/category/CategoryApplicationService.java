package com.ssi.application.category;

import com.ssi.application.category.model.CategoryListModel;
import com.ssi.application.category.model.CategoryModel;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
public interface CategoryApplicationService {

    List<CategoryListModel> getList();
    CategoryListModel saveOrUpdate(CategoryModel categoryModel);
    void delete(Long id);
    List<CategoryListModel> getChild(Long parentId);

}
