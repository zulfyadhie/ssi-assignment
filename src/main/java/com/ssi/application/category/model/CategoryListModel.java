package com.ssi.application.category.model;

/**
 * Created by zulfy on 20/05/16.
 */
public class CategoryListModel{

    private Long id;
    private String name;
    private String parentName;

    public CategoryListModel() {
    }

    public CategoryListModel(Long id, String name, String parentName) {
        this.id = id;
        this.name = name;
        this.parentName = parentName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}
