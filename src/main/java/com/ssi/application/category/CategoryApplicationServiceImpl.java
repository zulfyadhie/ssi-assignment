package com.ssi.application.category;

import com.ssi.application.category.model.CategoryListModel;
import com.ssi.application.category.model.CategoryModel;
import com.ssi.domain.category.Category;
import com.ssi.domain.category.CategoryDomainService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
@Component(value = "categoryApplicationService")
public class CategoryApplicationServiceImpl implements CategoryApplicationService{

    @Autowired
    private CategoryDomainService categoryDomainService;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, rollbackFor = Exception.class, timeout = 30)
    public List<CategoryListModel> getList() {
        List<CategoryListModel> dataList = new ArrayList<>();
        for(Category category : categoryDomainService.getAll()){
            dataList.add(new CategoryListModel(category.getId(), category.getName(), category.getParent() != null ? category.getParent().getName() : null));
        }
        return dataList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, timeout = 30)
    public CategoryListModel saveOrUpdate(CategoryModel categoryModel) {
        Category category = new Category();
        CategoryListModel categoryListModel = new CategoryListModel();
        BeanUtils.copyProperties(categoryModel, category);
        category.setParent(categoryModel.getParentId() != null ? categoryDomainService.findById(categoryModel.getParentId()) : null);
        category = categoryDomainService.saveOrUpdate(category);
        BeanUtils.copyProperties(category, categoryListModel);
        categoryListModel.setParentName(category.getParent() != null ? category.getParent().getName() : null);
        return categoryListModel;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, timeout = 30)
    public void delete(Long id) {
        categoryDomainService.delete(id);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, rollbackFor = Exception.class, timeout = 30)
    public List<CategoryListModel> getChild(Long parentId) {
        List<CategoryListModel> dataList = new ArrayList<>();
        for(Category category : categoryDomainService.getChild(parentId)){
            dataList.add(new CategoryListModel(category.getId(), category.getName(), category.getParent() != null ? category.getParent().getName() : null));
        }
        return dataList;
    }
}
