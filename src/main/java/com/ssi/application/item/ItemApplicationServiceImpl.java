package com.ssi.application.item;

import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.application.item.model.ItemListModel;
import com.ssi.application.item.model.ItemModel;
import com.ssi.domain.category.CategoryDomainService;
import com.ssi.domain.item.Item;
import com.ssi.domain.item.ItemDomainService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
@Component(value = "itemApplicationService")
public class ItemApplicationServiceImpl implements ItemApplicationService{

    @Autowired
    private ItemDomainService itemDomainService;
    @Autowired
    private CategoryDomainService categoryDomainService;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS, rollbackFor = Exception.class, timeout = 30)
    public List<ItemListModel> getList(ItemFilterModel itemFilterModel) {
        List<ItemListModel> dataList = new ArrayList<>();
        for(Item item : itemDomainService.getAll(itemFilterModel)){
            dataList.add(new ItemListModel(item.getId(), item.getName(), item.getColor(), item.getSize(), item.getPrice(), item.getCategory().getName()));
        }
        return dataList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, timeout = 30)
    public ItemListModel saveOrUpdate(ItemModel itemModel) {
        Item item = new Item();
        ItemListModel itemListModel = new ItemListModel();
        BeanUtils.copyProperties(itemModel, item);
        item.setCategory(categoryDomainService.findById(itemModel.getCategoryId()));
        item = itemDomainService.saveOrUpdate(item);
        BeanUtils.copyProperties(item, itemListModel);
        itemListModel.setCategoryName(item.getCategory().getName());
        return itemListModel;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, timeout = 30)
    public void delete(Long id) {
        itemDomainService.delete(id);
    }
}