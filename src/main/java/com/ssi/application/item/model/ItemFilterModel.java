package com.ssi.application.item.model;

import java.io.Serializable;

/**
 * Created by zulfy on 20/05/16.
 */
public class ItemFilterModel implements Serializable{

    private String size;
    private String color;
    private Long priceFrom;
    private Long priceTo;

    public ItemFilterModel() {
    }

    public ItemFilterModel(String size, String color, Long priceFrom, Long priceTo) {
        this.size = size;
        this.color = color;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(Long priceFrom) {
        this.priceFrom = priceFrom;
    }

    public Long getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Long priceTo) {
        this.priceTo = priceTo;
    }
}
