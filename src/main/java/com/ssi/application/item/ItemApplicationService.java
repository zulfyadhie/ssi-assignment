package com.ssi.application.item;

import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.application.item.model.ItemListModel;
import com.ssi.application.item.model.ItemModel;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
public interface ItemApplicationService{

    List<ItemListModel> getList(ItemFilterModel itemFilterModel);
    ItemListModel saveOrUpdate(ItemModel categoryModel);
    void delete(Long id);

}