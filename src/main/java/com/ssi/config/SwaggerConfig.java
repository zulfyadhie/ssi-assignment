package com.ssi.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.AuthorizationScopeBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.regex;


/**
 * Created by zulfy on 19/05/16.
 */
@Configuration
@EnableSwagger2
@ComponentScan
public class SwaggerConfig {

    @Bean
    public Docket reportsAPI() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("catalog")
                .apiInfo(apiInfo())
                .select()
                .paths(reportsPath())
                .build();
    }

    private Predicate<String> reportsPath() {
        return regex("/catalog.*");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SSI assignment")
                .description("This API is developed for SSI assignment")
                .version("1.0")
                .build();
    }
}