package com.ssi.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by zulfy on 19/05/16.
 */
@Configuration
public class DataSourceConfig {

    @Autowired
    private DataSourceProperties dataSourceProperties;

    private Logger log = LoggerFactory.getLogger(DataSourceConfig.class);

    @Bean
    @Primary
    public DataSource hikariDataSource(){
        Properties props = new Properties();
        props.setProperty("dataSourceClassName", dataSourceProperties.getDataSourceClassName());
        props.setProperty("dataSource.user", dataSourceProperties.getUsername());
        props.setProperty("dataSource.password", dataSourceProperties.getPassword());
        props.setProperty("dataSource.url", dataSourceProperties.getUrl());
        props.setProperty("minimumIdle", dataSourceProperties.getMinimumIdle());
        props.setProperty("maximumPoolSize", dataSourceProperties.getMaximumPoolSize());

        HikariConfig hc = new HikariConfig(props);
        HikariDataSource ds = new HikariDataSource(hc);
        return ds;
    }

}