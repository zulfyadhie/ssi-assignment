package com.ssi.domain.category;

import com.ssi.domain.exception.BusinessException;
import com.ssi.infrastructure.repository.CategoryRepository;
import com.ssi.infrastructure.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
@Service(value = "categoryDomainService")
public class CategoryDomainServiceImpl implements CategoryDomainService{

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ItemRepository itemRepository;

    @Override
    public Category findById(Long id) {
        return categoryRepository.findOne(id);
    }

    @Override
    public Category saveOrUpdate(Category entity) {
        Category category;
        if(entity.getId() != null){
            category = categoryRepository.findOne(entity.getId());
            category.setName(entity.getName());
            category.setParent(entity.getParent());
        }else{
            category = entity;
        }
        return categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        if(categoryRepository.findOne(id) == null){
            throw new BusinessException("Category not found");
        }else if(categoryRepository.findByParentId(id).size() > 0){
            throw new BusinessException("Please delete child category first");
        }else if(itemRepository.findByCategoryId(id).size() > 0){
            throw new BusinessException("Please delete item first");
        }
        categoryRepository.delete(id);
    }

    @Override
    public Iterable<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Category> getChild(Long parentId) {
        return categoryRepository.findByParentId(parentId);
    }
}