package com.ssi.domain.category;

import com.ssi.domain.BaseDomainService;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
public interface CategoryDomainService extends BaseDomainService<Category, Long>{

    List<Category> getChild(Long parentId);

}