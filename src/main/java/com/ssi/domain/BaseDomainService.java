package com.ssi.domain;

/**
 * Created by zulfy on 20/05/16.
 */
public interface BaseDomainService<T, I> {

    T findById(I id);

    T saveOrUpdate(T entity);

    void delete(I id);

    Iterable<T> getAll();

}