package com.ssi.domain.exception;

/**
 * Created by zulfy on 20/05/16.
 */
public class BusinessException extends RuntimeException{

    public BusinessException(String message) {
        super(message);
    }
}
