package com.ssi.domain.item;

import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.domain.BaseDomainService;

/**
 * Created by zulfy on 20/05/16.
 */
public interface ItemDomainService extends BaseDomainService<Item, Long>{

    Iterable<Item> getAll(ItemFilterModel itemFilterModel);

}