package com.ssi.domain.item;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.domain.exception.BusinessException;
import com.ssi.infrastructure.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.ssi.domain.item.QItem.item;

/**
 * Created by zulfy on 20/05/16.
 */
@Service(value = "itemDomainService")
public class ItemDomainServiceImpl implements ItemDomainService {

    @Autowired
    private ItemRepository itemRepository;
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Item findById(Long id) {
        return itemRepository.findOne(id);
    }

    @Override
    public Item saveOrUpdate(Item entity) throws BusinessException{
        Item item;
        if(entity.getId() != null){
            item = itemRepository.findOne(entity.getId());
            item.setName(entity.getName());
            item.setCategory(entity.getCategory());
            item.setColor(entity.getColor());
            item.setPrice(entity.getPrice());
            item.setSize(entity.getSize());
        }else{
            item = entity;
        }
        return itemRepository.save(item);
    }

    @Override
    public void delete(Long id) {
        itemRepository.delete(id);
    }

    @Override
    public Iterable<Item> getAll() {
        return itemRepository.findAll();
    }

    @Override
    public Iterable<Item> getAll(ItemFilterModel itemFilterModel) {
        JPAQuery query = new JPAQuery(entityManager).from(item);
        BooleanBuilder builder = new BooleanBuilder();
        if(itemFilterModel.getSize() != null) {
            builder.and(item.size.equalsIgnoreCase(itemFilterModel.getSize()));
        }
        if(itemFilterModel.getColor() != null) {
            builder.and(item.color.equalsIgnoreCase(itemFilterModel.getColor()));
        }
        if(itemFilterModel.getPriceFrom() != null && itemFilterModel.getPriceTo() != null){
            builder.and(item.price.between(itemFilterModel.getPriceFrom(), itemFilterModel.getPriceTo()));
        }
        return query.where(builder).list(item);
    }
}