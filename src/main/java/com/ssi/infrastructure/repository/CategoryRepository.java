package com.ssi.infrastructure.repository;

import com.ssi.domain.category.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findByParentId(Long parentId);

}
