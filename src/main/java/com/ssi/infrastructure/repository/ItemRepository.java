package com.ssi.infrastructure.repository;

import com.ssi.domain.item.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by zulfy on 20/05/16.
 */
public interface ItemRepository extends CrudRepository<Item, Long> {

    List<Item> findByCategoryId(Long categoryId);

}
