package com.ssi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssi.application.category.model.CategoryListModel;
import com.ssi.application.category.model.CategoryModel;
import com.ssi.application.item.model.ItemFilterModel;
import com.ssi.application.item.model.ItemListModel;
import com.ssi.application.item.model.ItemModel;
import com.ssi.infrastructure.repository.CategoryRepository;
import com.sun.deploy.net.HttpResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SsiAssignmentApplicationTests.class)
@WebAppConfiguration
public class SsiAssignmentApplicationTests {

	RestTemplate restTemplate = new TestRestTemplate();

    private String baseUrl = "http://localhost:8080";

	@Test
	public void catalogTest() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        // create category with no parent
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setName("Testing Category");

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(baseUrl + "/catalog/category", categoryModel, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        CategoryListModel categoryListModel = mapper.readValue(responseEntity.getBody(), CategoryListModel.class);

        // create category with parent
        categoryModel.setParentId(categoryListModel.getId());
        categoryModel.setName("Testing Child");
        responseEntity = restTemplate.postForEntity(baseUrl + "/catalog/category", categoryModel, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        CategoryListModel categoryListModelChild = mapper.readValue(responseEntity.getBody(), CategoryListModel.class);
        Assert.assertEquals(categoryListModel.getName(), categoryListModelChild.getParentName());

        // update category
        categoryModel.setName("Testing Child edit");
        responseEntity = restTemplate.postForEntity(baseUrl + "/catalog/category", categoryModel, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        categoryListModel = mapper.readValue(responseEntity.getBody(), CategoryListModel.class);
        Assert.assertEquals(categoryListModel.getName(), "Testing Child edit");

        // create Item
        ItemModel itemModel = new ItemModel();
        itemModel.setName("Testing item");
        itemModel.setCategoryId(categoryListModel.getId());
        itemModel.setColor("Black black black");
        itemModel.setSize("XXXL");
        itemModel.setPrice(123456789L);
        responseEntity = restTemplate.postForEntity(baseUrl + "/catalog/item", itemModel, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        ItemListModel itemListModel = mapper.readValue(responseEntity.getBody(), ItemListModel.class);

        // update item
        itemModel.setId(itemListModel.getId());
        itemModel.setName("Testing item Update");
        responseEntity = restTemplate.postForEntity(baseUrl + "/catalog/item", itemModel, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        // filter
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("size", itemModel.getSize());
        filterMap.put("color", itemModel.getColor());
        filterMap.put("priceFrom", 0L);
        filterMap.put("priceTo", 123456790L);
        responseEntity = restTemplate.getForEntity(baseUrl + "/catalog/item", String.class, filterMap);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List filterList = mapper.readValue(responseEntity.getBody(), List.class);
        Assert.assertTrue(filterList.size() > 0);

        // delete item
        restTemplate.delete(baseUrl + "/catalog/item/" + itemListModel.getId());

        // delete child
        restTemplate.delete(baseUrl + "/catalog/category/" + categoryListModelChild.getId());
        // delete parent
        restTemplate.delete(baseUrl + "/catalog/category/" + categoryListModel.getId());
    }



}
